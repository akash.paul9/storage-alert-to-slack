#!/bin/bash

VAR=$(df / | grep / | awk '{ print $5}' | sed 's/%//g')

if [[ $VAR -gt 80 ]]
then
  curl -X POST --data-urlencode "payload={\"channel\": \"#sms-dipping-storage\", \"username\": \"storage-alert\", \"text\": \" Storage is $VAR% full. Please clean up storage.\", \"icon_emoji\": \":ghost:\"}" https://hooks.slack.com/services/xxxxxxxxxxxxxxxx
fi
